# Rakefile
# Author:: Pascal Morillon (<pascal.morillon@irisa.fr>)
# Author:: Sebastien Badia (<sebastien.badia@inria.fr>)
# Date:: Thu Feb 15 16:26:36 +0100 2011
#

require 'rake'
require 'yaml'
require 'ipaddress'
require 'date'

SITE = ENV['SITE']
REF_REPO_DIR = if Dir::exist?('../reference-repository')
  '../reference-repository'
elsif ENV['GRID5000_GIT_DIR'] && Dir::exist?(ENV['GRID5000_GIT_DIR']  + '/reference-repository')
  ENV['GRID5000_GIT_DIR']  + '/reference-repository'
elsif ENV['REFERENCE_REPO'] && Dir::exist?(ENV['REFERENCE_REPO'])
  ENV['REFERENCE_REPO']
else
 raise "Cannot find reference-repository directory, please set the $REFERENCE_REPO environment variable"
end
CONF_IPV4 = YAML::load(File.open(REF_REPO_DIR + "/input/grid5000/ipv4.yaml"))
# We need that to expand 
require_relative REF_REPO_DIR + '/lib/refrepo/hash/hash'

# load a yaml file in a way compatible with both Ruby 3.1 and 2.7
def yaml_load_compat(f)
  load_args = {}
  if ::Gem::Version.new(RUBY_VERSION) >= ::Gem::Version.new("3.1.0")
    # Fix compatibility with ruby 3.1
    load_args[:permitted_classes] = [Date, Time]
    load_args[:aliases] = true
  end
  return YAML::load(f, **load_args)
end

verbose(true)

desc "Setup the SSH host keys for the cluster nodes (to be done only once)"
task :setup do
  raise "MUST provide a SITE=<sitename>" unless ENV['SITE']
  raise "MUST provide a CLUSTER=<clustername | all>" unless ENV['CLUSTER']
  init(ENV['SITE'], ENV['CLUSTER'])
  @iface_offsets.each do|cluster_info, offset|
    site, cluster, _ = cluster_info
    puts "--> configuring cluster #{cluster}"
    cluster_data = yaml_load_compat(File.open(REF_REPO_DIR + "/input/grid5000/sites/#{site}/clusters/#{cluster}/#{cluster}.yaml")).clone.expand_square_brackets()

    cluster_data['nodes'].each do |hostname,v|
      next if v['status'] == 'retired'
      node_id = hostname.split('-')[1].to_i
      ip = IPAddress::IPv4::parse_u32(@base_ip + @site_offset + offset + node_id).to_s
      puts "    #{hostname} (#{ip})"
      sshdir  = "ssh_host_keys/#{SITE}/#{cluster}/#{ip}"
      puts "    => creating directory #{sshdir}"
      sh "mkdir -p #{sshdir}"
      puts "    => creating SSH host key (rsa) for #{hostname}"
      sh "ssh-keygen -t rsa -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_rsa_key"
      puts "    => creating SSH host key (ecdsa) for #{hostname}"
      sh "ssh-keygen -t ecdsa -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_ecdsa_key"
      puts "    => creating SSH host key (ed25519) for #{hostname}"
      sh "ssh-keygen -t ed25519 -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_ed25519_key"
    end
  end
end

desc "Clean the SSH host keys for the cluster nodes"
task :clean do
  raise "MUST provide a SITE=<sitename>" unless ENV['SITE']
  raise "MUST provide a CLUSTER=<clustername | all>" unless ENV['CLUSTER']
  init(ENV['SITE'], ENV['CLUSTER'])
  @iface_offsets.each do |cluster_info, _|
    site, cluster, _ = cluster_info
    puts "--> deleting cluster #{cluster}"
    dir="ssh_host_keys/#{site}/#{cluster}"
    puts "=> deleting #{dir}/"
    sh "rm -rf #{dir}" if File.exist?("#{dir}")
  end
end

desc "Clean retired node"
task :autoclean do
  CONF_IPV4['ipv4']["sites_offsets"].split("\n").map{|x| x.split().first}.each do |site|
    init(site)
    @iface_offsets.each do|cluster_info,offset|
      site, cluster, _ = cluster_info
      cluster_data = yaml_load_compat(File.open(REF_REPO_DIR + "/input/grid5000/sites/#{site}/clusters/#{cluster}/#{cluster}.yaml")).clone.expand_square_brackets()
      cluster_data['nodes'].each do |hostname,v|
        node_id = hostname.split('-')[1].to_i
        ip = IPAddress::IPv4::parse_u32(@base_ip + @site_offset + offset + node_id).to_s
        ssh_file="ssh_host_keys/#{site}/#{cluster}/#{ip}"
        if v['status'] == 'retired' && File.exist?("#{ssh_file}")
          puts "=> deleting retired node #{hostname}: #{ssh_file}"
          sh "rm -rf #{ssh_file}" if File.exist?("#{ssh_file}")
        end
      end
    end
  end
end

def init(site, cluster='all')
  @base_ip = IPAddress::IPv4::new(CONF_IPV4['ipv4']['base']).to_u32
  @site_offset = CONF_IPV4['ipv4']['sites_offsets'].split("\n").map { |l| l = l.split(/\s+/) ; [ l[0], l[1..-1].inject(0) { |a, b| (a << 8) + b.to_i } ] }.to_h.fetch(site)
  @iface_offsets = CONF_IPV4['ipv4']['iface_offsets'].split("\n").map { |l| l = l.split(/\s+/) ; [ l[0..2], l[3..-1].inject(0) { |a, b| (a << 8) + b.to_i } ] }.to_h.select{|k,_| k[0] == site}
  @iface_offsets.select!{|k,_| k[2] == 'eth0'}

  if cluster != "all"
    @iface_offsets.select!{|k,_| k[1] == cluster}
    raise "No cluster \"cluster\" found." if @iface_offsets.empty?
  end
end
