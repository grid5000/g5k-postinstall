class MultiLogger
  def initialize(file, dev)
    @file = file
    @dev = dev
  end

  def write(str)
    str2 = str.gsub(/\n$/, "\r\n") # hack: add \r so that the console is properly formatted
    @file.write(str) ; @file.flush
    @dev.write(str2) ; @dev.flush
  end

  def close
    @file.close
    @dev.close
  end
end
