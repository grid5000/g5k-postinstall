require 'logger'
require 'optparse'
require 'erb'
require 'json'
require 'fileutils'
require 'yaml'

def system_or_raise(cmd)
  system(cmd) or raise("Command failed: #{cmd}")
end

def check_chroot(dstdir)
  system("chroot #{dstdir} /bin/true") or raise("Cannot chroot in the deployed environment: invalid system or wrong architecture?")
end

require 'g5k-postinstall/config'
require 'g5k-postinstall/multiio'
require 'g5k-postinstall/node-info'
require 'g5k-postinstall/node-status'
require 'g5k-postinstall/hacks'
