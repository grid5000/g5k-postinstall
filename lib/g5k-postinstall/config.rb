# Mapping between IP ranges and sites (used to determine current site)

SITE_IP_RANGE = [
  ['grenoble', '172.16.16.0/20'],
  ['lille', '172.16.32.0/20'],
  ['lyon', '172.16.48.0/20'],
  ['nancy', '172.16.64.0/20'],
  ['rennes', '172.16.96.0/20'],
  ['toulouse', '172.16.112.0/20'],
  ['sophia', '172.16.128.0/20'],
  ['strasbourg', '172.16.160.0/20'],
  ['luxembourg', '172.16.176.0/20'],
  ['nantes', '172.16.192.0/20'],
  ['louvain', '172.16.208.0/20']
]
